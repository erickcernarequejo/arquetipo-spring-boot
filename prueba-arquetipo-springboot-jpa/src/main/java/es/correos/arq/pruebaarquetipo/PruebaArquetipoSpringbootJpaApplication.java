package es.correos.arq.pruebaarquetipo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaArquetipoSpringbootJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaArquetipoSpringbootJpaApplication.class, args);
	}

}
